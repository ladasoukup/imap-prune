﻿using System;
using System.Collections.Generic;
using System.Text;
using InterIMAP;
using CommandLine.Utility;
using System.Reflection;


namespace IMAP_prune
{
    class Program
    {
        static void Main(string[] args)
        {
            bool allParams = true;
            string IMAP_server = "";
            string IMAP_user = "";
            string IMAP_pwd = "";
            string IMAP_folder = "";
            int IMAP_prune_hours = 720; // 30 dnu

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("IMAP prune");
            Console.WriteLine("===========================================================================");
 
            Arguments CommandLine = new Arguments(args);

            if (CommandLine["server"] != null) IMAP_server = CommandLine["server"];
            else allParams = false;

            if (CommandLine["user"] != null) IMAP_user = CommandLine["user"];
            else allParams = false;

            if (CommandLine["pwd"] != null) IMAP_pwd = CommandLine["pwd"];
            else allParams = false;

            if (CommandLine["folder"] != null) IMAP_folder = CommandLine["folder"];
            
             
            if ((CommandLine["hours"] != null) || (CommandLine["days"] != null))
                IMAP_prune_hours = 0;

            if (CommandLine["hours"] != null) IMAP_prune_hours += Convert.ToInt32(CommandLine["hours"]);
            if (CommandLine["days"] != null) IMAP_prune_hours += (24 * Convert.ToInt32(CommandLine["days"]));

            if (allParams == false) showHelp();
            else DoPrune(IMAP_server, IMAP_user, IMAP_pwd, IMAP_folder, IMAP_prune_hours);


            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("");
            System.Threading.Thread.Sleep(5000);
            // EXIT
        }
        static void showHelp() {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Some arguments are missing...");
            Console.WriteLine("");
            Console.WriteLine("[USING]");
            Console.WriteLine("");
            Console.WriteLine("IMAP_prune.exe /server:<SERVER> /user:<IMAP_USER_NAME> /pwd:<IMAP_PASSWORD> /folder:<IMAP_FOLDER> [/hours:<HOURS>] [/days:<DAYS>]");
            Console.WriteLine("");
            Console.WriteLine("/server\t - IMAP server hostname (ex. mail.example.com)");
            Console.WriteLine("/user\t - IMAP username (ex. user@example.com)");
            Console.WriteLine("/pwd\t - IMAP password");
            Console.WriteLine("/folder\t - IMAP folder (ex. INBOX)");
            Console.WriteLine("\t - folder names are CASE sensitive!");
            Console.WriteLine("\t - leave empty or not set to list all availible folders for account.");

            Console.WriteLine("");
            Console.WriteLine("/hours\t - delete all email odler then this value (hours)");
            Console.WriteLine("/days\t - delete all email odler then this value (days)");
            Console.WriteLine("\t Note: hours and days will be added together. Default value is 30 days.");
            Console.WriteLine("");
            Console.WriteLine("");
            // DEBUG
            // DoPrune("mail.radiocity.cz", "redakce8@radioblanik.cz", "novosti", "INBOX", 12);

        }
        static void DoPrune(string IMAP_server, string IMAP_user, string IMAP_pwd, string IMAP_folder, int IMAP_prune_hours)
        {
            IMAPConfig I_config = null;
            IMAPClient I_client = null;
            IMAPFolder I_folder = null;
            bool skipOtherSteps = false;

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("IMAP server:    " + IMAP_server);
            Console.WriteLine("IMAP user:      " + IMAP_user);
            Console.WriteLine("IMAP folder:    " + IMAP_folder);
            Console.WriteLine("Prune interval: " + IMAP_prune_hours.ToString() + " hours");

            Console.WriteLine("---------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Gray;

            try
            {
                I_config = new IMAPConfig(IMAP_server, IMAP_user, IMAP_pwd, false, false, "");
                I_client = new IMAPClient(I_config, null, 5);
                I_client.Config.DebugDetail = IMAPConfig.DebugDetailLevel.ErrorsOnly;
                I_client.Config.DebugMode = false;

                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("---------------------------------------------------------------------------");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Connecting to IMAP server...");
                I_client.Logon();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("[IMAP server connected]");
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("[Connection failed]");
                skipOtherSteps = true;
            }

            if (skipOtherSteps == false)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Working, please wait...");

                DateTime searchFrom = DateTime.MinValue;
                DateTime searchTo = DateTime.Now.AddHours(-IMAP_prune_hours);

                Console.WriteLine("Deleting messages older than " + searchTo.ToString());
                System.Threading.Thread.Sleep(2500);

                

                I_folder = I_client.Folders[IMAP_folder];
                try
                {
                    IMAPSearchResult msgs = I_folder.Search(IMAPSearchQuery.QuickSearchDateRange(searchFrom, searchTo));
                    foreach (IMAPMessage msg in msgs.Messages)
                    {
                        try
                        {
                            if (msg.Subject != null) Console.WriteLine("Deleting: " + msg.Subject.ToString());
                            else Console.WriteLine("Deleting msg without subject");

                            I_folder.DeleteMessage(msg);
                        }
                        catch (Exception ex)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("error: " + ex.ToString());
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("");
                        }
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("error: IMAP folder not found");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("");
                    Console.WriteLine("");
                    try
                    {
                        Console.WriteLine("IMAP folders for this account:");
                        Console.WriteLine("==============================");
                        foreach (IMAPFolder folder in I_client.Folders)
                        {
                            Console.WriteLine(folder.FolderPath.ToString());
                        }
                        Console.WriteLine("==============================");
                    }
                    catch { }
                }


            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Done.");
        }
    }
}
